package com.mehmet6parmak.training.ui.viewgroups;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.mehmet6parmak.training.ui.R;

public class RelativeLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative_layout);
    }

    public void addTextClicked(View view) {
        TextView txt = (TextView) findViewById(R.id.TextView05);
        String newText = txt.getText().toString() + "+";
        txt.setText(newText);
    }
}
