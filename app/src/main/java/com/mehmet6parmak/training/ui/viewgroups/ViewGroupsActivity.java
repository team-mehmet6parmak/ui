package com.mehmet6parmak.training.ui.viewgroups;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.mehmet6parmak.training.ui.R;

public class ViewGroupsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_groups);
    }

    public void frameLayoutClicked(View view) {
        startActivity(new Intent(this, FrameLayoutActivity.class));
    }

    public void linearLayoutClicked(View view) {
        startActivity(new Intent(this, LinearLayoutActivity.class));
    }

    public void linearLayoutWeightedClicked(View view) {
        startActivity(new Intent(this, LinearLayoutWeightedActivity.class));
    }

    public void linearLayoutWeighted2Clicked(View view) {
        startActivity(new Intent(this, LinearLayoutWeighted2Activity.class));
    }

    public void relativeLayoutClicked(View view) {
        startActivity(new Intent(this, RelativeLayoutActivity.class));
    }
}
