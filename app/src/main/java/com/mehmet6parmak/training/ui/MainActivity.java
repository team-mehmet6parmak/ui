package com.mehmet6parmak.training.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.mehmet6parmak.training.ui.adapterviews.AdapterViewsActivity;
import com.mehmet6parmak.training.ui.dialog.DialogsActivity;
import com.mehmet6parmak.training.ui.input.WidgetsActivity;
import com.mehmet6parmak.training.ui.menu.MenuActivity;
import com.mehmet6parmak.training.ui.notification.NotificationsActivity;
import com.mehmet6parmak.training.ui.viewgroups.ViewGroupsActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void viewGroupsClicked(View view) {
        startActivity(new Intent(this, ViewGroupsActivity.class));
    }

    public void inputControlsClicked(View view) {
        startActivity(new Intent(this, WidgetsActivity.class));
    }

    public void dialogsClicked(View view) {
        startActivity(new Intent(this, DialogsActivity.class));
    }

    public void menusClicked(View view) {
        startActivity(new Intent(this, MenuActivity.class));
    }

    public void notificationsClicked(View view) {
        startActivity(new Intent(this, NotificationsActivity.class));
    }

    public void onAdapterViewsClicked(View view) {
        startActivity(new Intent(this, AdapterViewsActivity.class));
    }
}
