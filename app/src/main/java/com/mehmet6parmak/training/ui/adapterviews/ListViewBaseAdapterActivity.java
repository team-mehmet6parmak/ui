package com.mehmet6parmak.training.ui.adapterviews;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mehmet6parmak.training.ui.R;
import com.mehmet6parmak.training.ui.adapterviews.model.SimpleAdapterModel;

import java.util.ArrayList;
import java.util.List;

public class ListViewBaseAdapterActivity extends AppCompatActivity {

    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_base_adapter);

        list = (ListView) findViewById(R.id.list);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SimpleAdapterModel model = (SimpleAdapterModel) parent.getAdapter().getItem(position);
            }
        });
        list.setAdapter(new BaseAdapterImpl(generateDummyData()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_view_base_adapter, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_change_adapter);
        if (list.getAdapter() instanceof BaseAdapterImpl) {
            item.setTitle("Multiple View Types");
        } else {
            item.setTitle("Single View Type");
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_change_adapter) {
            if (list.getAdapter() instanceof BaseAdapterImpl) {
                list.setAdapter(new BaseAdapterImplMultipleViewTypes(generateDummyData()));
            } else {
                list.setAdapter(new BaseAdapterImpl(generateDummyData()));
            }
        } else if (item.getItemId() == R.id.menu_add_new_item) {
            BaseAdapterImpl adapter = (BaseAdapterImpl) list.getAdapter();
            adapter.insert(new SimpleAdapterModel("Newly Added", "New born."));
        }
        return super.onOptionsItemSelected(item);
    }

    public List<SimpleAdapterModel> generateDummyData() {
        List<SimpleAdapterModel> data = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            data.add(new SimpleAdapterModel("Title - " + i, "Subtitle - " + i));
        }
        return data;
    }

    class BaseAdapterImpl extends BaseAdapter {

        protected List<SimpleAdapterModel> data;

        public BaseAdapterImpl(List<SimpleAdapterModel> data) {
            this.data = data;
        }

        public void insert(SimpleAdapterModel simpleAdapterModel) {
            data.add(0, simpleAdapterModel);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(ListViewBaseAdapterActivity.this);
                convertView = inflater.inflate(R.layout.list_item_adapter, parent, false);
            }

            TextView txtTitle = (TextView) convertView.findViewById(R.id.txt_title);
            TextView txtSubTitle = (TextView) convertView.findViewById(R.id.txt_subtitle);

            SimpleAdapterModel model = (SimpleAdapterModel) getItem(position);

            txtTitle.setText(model.getTitle());
            txtSubTitle.setText(model.getSubTitle());

            return convertView;
        }
    }

    public class BaseAdapterImplMultipleViewTypes extends BaseAdapterImpl {

        public BaseAdapterImplMultipleViewTypes(List<SimpleAdapterModel> data) {
            super(data);
        }

        @Override
        public int getItemViewType(int position) {
            return position % 2;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int viewType = getItemViewType(position);
            ViewHolder holder;
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(ListViewBaseAdapterActivity.this);
                if (viewType == 0) {
                    convertView = inflater.inflate(R.layout.list_item_adapter, parent, false);
                } else {
                    convertView = inflater.inflate(R.layout.list_item_adapter_huge, parent, false);
                }

                holder = new ViewHolder();
                holder.txtTitle = (TextView) convertView.findViewById(R.id.txt_title);
                holder.txtSubTitle = (TextView) convertView.findViewById(R.id.txt_subtitle);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            SimpleAdapterModel model = (SimpleAdapterModel) getItem(position);

            holder.txtTitle.setText(model.getTitle());
            holder.txtSubTitle.setText(model.getSubTitle());

            return convertView;
        }
    }

    static class ViewHolder {
        TextView txtTitle;
        TextView txtSubTitle;
    }
}
