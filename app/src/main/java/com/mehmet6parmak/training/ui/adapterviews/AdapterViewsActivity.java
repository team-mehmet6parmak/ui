package com.mehmet6parmak.training.ui.adapterviews;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mehmet6parmak.training.ui.R;

public class AdapterViewsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter_views);
    }

    public void onListViewClicked(View view) {
        startActivity(new Intent(this, ListViewActivity.class));
    }

    public void onGridViewClicked(View view) {
        startActivity(new Intent(this, GridViewActivity.class));
    }

    public void onSpinnerClicked(View view) {
        startActivity(new Intent(this, SpinnerActivity.class));
    }

    public void onListViewBaseAdapterClicked(View view) {
        startActivity(new Intent(this, ListViewBaseAdapterActivity.class));
    }

    public void onListViewCursorAdapterClicked(View view) {
        startActivity(new Intent(this, ListViewCursorAdapterActivity.class));
    }

    public void onRecyclerViewClicked(View view) {
        startActivity(new Intent(this, RecyclerViewActivity.class));
    }

    class SampleAdapter extends BaseAdapter {
        @Override
        public int getCount() {return 0;}

        @Override
        public Object getItem(int position) {return null;}

        @Override
        public long getItemId(int position) {return 0L;}

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {return null;}
    }
}
