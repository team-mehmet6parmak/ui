package com.mehmet6parmak.training.ui.adapterviews;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mehmet6parmak.training.ui.R;
import com.mehmet6parmak.training.ui.adapterviews.model.SimpleAdapterModel;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new RecyclerAdapter());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_recycler_view, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_recycler_horizontal) {
            item.setChecked(!item.isChecked());
            if (item.isChecked()) {
                recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                recyclerView.setAdapter(new RecyclerAdapter());
            } else {
                recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                recyclerView.setAdapter(new RecyclerAdapter());
            }
        } else if (item.getItemId() == R.id.menu_recycler_grid) {
            item.setChecked(!item.isChecked());
            if (item.isChecked()) {
                GridLayoutManager manager = new GridLayoutManager(this, 3);
                recyclerView.setLayoutManager(manager);
                recyclerView.setAdapter(new RecyclerAdapter());
            } else {
                recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                recyclerView.setAdapter(new RecyclerAdapter());
            }
        } else if (item.getItemId() == R.id.menu_recycler_insert) {
            RecyclerAdapter adapter = (RecyclerAdapter) recyclerView.getAdapter();
            adapter.insertNew();
        } else if (item.getItemId() == R.id.menu_recycler_animate) {
            item.setChecked(!item.isChecked());
            if (item.isChecked()) {
                recyclerView.setItemAnimator(new DefaultItemAnimator());
            } else {
                recyclerView.setItemAnimator(null);
            }
        } else if (item.getItemId() == R.id.menu_recycler_remove) {
            RecyclerAdapter adapter = (RecyclerAdapter) recyclerView.getAdapter();
            adapter.removeFirst();
        }
        return super.onOptionsItemSelected(item);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt;
        public ViewHolder(View itemView) {
            super(itemView);
            txt = (TextView) itemView.findViewById(android.R.id.text1);
        }
    }

    class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {
        public List<SimpleAdapterModel> simpleAdapterModels;

        public RecyclerAdapter() {
            simpleAdapterModels = SimpleAdapterModel.generateDummyData();
        }

        public void insertNew() {
            SimpleAdapterModel model = new SimpleAdapterModel("New Item", "Newborn");
            simpleAdapterModels.add(0, model);

            notifyItemInserted(0);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(RecyclerViewActivity.this);
            View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    SimpleAdapterModel model = simpleAdapterModels.get(position);

                    Toast.makeText(RecyclerViewActivity.this, "Clicked: " + model.getTitle(), Toast.LENGTH_SHORT).show();
                }
            });

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.itemView.setTag(position);
            holder.txt.setText(simpleAdapterModels.get(position).getTitle());
        }

        @Override
        public int getItemCount() {
            return simpleAdapterModels.size();
        }

        public void removeFirst() {
            simpleAdapterModels.remove(0);
            notifyItemRemoved(0);
        }
    }
}
