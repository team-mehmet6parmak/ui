package com.mehmet6parmak.training.ui.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mehmet6parmak.training.ui.R;

public class DialogsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialogs);
    }

    public void onAlertDialogClicked(View view) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(DialogsActivity.this);
        dialog.setTitle("Hello World");
        dialog.setMessage("This is message");
        dialog.setView(R.layout.dialog_alert);
        dialog.setPositiveButton("Positive", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.setIcon(R.mipmap.ic_launcher);
        //dialog.setItems()
        //dialog.setCancelable(false);
        //dialog.setAdapter();
        //dialog.setNegativeButton();
        //dialog.setNeutralButton()
        //dialog.setCursor()
        dialog.create().show();


    }

    public void onDialogFragmentClicked(View view) {
        BasicDialogFragment dialogFragment = new BasicDialogFragment();
        dialogFragment.show(getSupportFragmentManager(), "");
    }
}
