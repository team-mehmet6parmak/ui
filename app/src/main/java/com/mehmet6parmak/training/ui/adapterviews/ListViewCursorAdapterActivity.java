package com.mehmet6parmak.training.ui.adapterviews;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.mehmet6parmak.training.ui.R;

public class ListViewCursorAdapterActivity extends AppCompatActivity {

    ListView list;

    private static final String[] PROJECTION = new String [] {
            ContactsContract.Contacts._ID,
            ContactsContract.Contacts.DISPLAY_NAME
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_cursor_adapter);

        list = (ListView) findViewById(R.id.list);
        list.setFastScrollEnabled(true);
        list.setFastScrollAlwaysVisible(true);

        Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, PROJECTION, null, null, ContactsContract.Contacts.DISPLAY_NAME);
        list.setAdapter(new CursorAdapterImpl(this, cursor, true));
    }


    public class CursorAdapterImpl extends CursorAdapter{
        public CursorAdapterImpl(Context context, Cursor c, boolean autoRequery) {
            super(context, c, autoRequery);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(ListViewCursorAdapterActivity.this);
            return inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView txt = (TextView) view.findViewById(android.R.id.text1);
            txt.setText(cursor.getString(1));
        }
    }
}
