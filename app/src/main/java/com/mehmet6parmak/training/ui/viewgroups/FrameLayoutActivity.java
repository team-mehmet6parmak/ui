package com.mehmet6parmak.training.ui.viewgroups;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mehmet6parmak.training.ui.R;

public class FrameLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_layout);
    }
}
