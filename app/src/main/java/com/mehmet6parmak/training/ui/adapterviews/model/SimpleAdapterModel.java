package com.mehmet6parmak.training.ui.adapterviews.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maltiparmak on 11.10.2016.
 */

public class SimpleAdapterModel {

    private String title;
    private String subTitle;

    public SimpleAdapterModel(String title, String subTitle) {
        this.title = title;
        this.subTitle = subTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }


    public static List<SimpleAdapterModel> generateDummyData() {
        List<SimpleAdapterModel> data = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            data.add(new SimpleAdapterModel("Title - " + i, "Subtitle - " + i));
        }
        return data;
    }
}
