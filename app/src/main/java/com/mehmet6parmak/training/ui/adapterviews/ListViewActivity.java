package com.mehmet6parmak.training.ui.adapterviews;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.mehmet6parmak.training.ui.R;

public class ListViewActivity extends AppCompatActivity {

    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        list = (ListView) findViewById(R.id.list);

        list.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.countries)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_listview, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_add_header_view) {
            list.addHeaderView(LayoutInflater.from(this).inflate(R.layout.layout_header, null));
        } else if (item.getItemId() == R.id.menu_add_footer_view) {
            list.addFooterView(LayoutInflater.from(this).inflate(R.layout.layout_footer, null));
        }
        return super.onOptionsItemSelected(item);
    }
}
